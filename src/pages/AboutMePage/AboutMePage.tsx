import { IonCol, IonGrid, IonPage, IonRow } from "@ionic/react";
import React from "react";
import { Me } from "../../components/me/me";
import './AboutMe.css';

export const AboutMePage: React.FC = () => {

  return (
    <IonPage>
      <IonGrid fixed>
        <IonRow className="ion-justify-content-cente" >
          <IonCol sizeSm="12" sizeMd="6" sizeLg="4">
            <Me />
          </IonCol>
          <IonCol>

          </IonCol>
        </IonRow>
      </IonGrid>
    </IonPage>
  )
}