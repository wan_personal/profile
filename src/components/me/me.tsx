import { IonAvatar, IonCard, IonCardContent, IonChip, IonIcon, IonItem, IonLabel } from "@ionic/react";
import { logoGithub, logoGitlab, logoLinkedin } from 'ionicons/icons';
import React from "react";
import './me.css';

export const Me: React.FC = () => {


  return (
    <>
      <IonItem>
        <IonAvatar slot="start">
          <img src="assets/img/20190122_075430.jpg" />
        </IonAvatar>
        <IonLabel>
          <h2>Juan Salinas Zavaleta</h2>
          <h3>@WansoftOne</h3>
          <div className="contact-row">
            <a href="https://www.linkedin.com/in/wansoft-developer" className="contact" target="blank">
              <IonIcon icon={logoLinkedin} size="40px" /> 
            </a>
            <a href="https://github.com/WansoftOne" className="contact" target="blank">
              <IonIcon icon={logoGithub} size="40px" color="dark" /> 
            </a>
            <a href="https://gitlab.com/WansoftOne" className="contact" target="blank">
              <IonIcon icon={logoGitlab} size="40px" color="orange"/> 
            </a>
          </div>
          </IonLabel>
      </IonItem>
      <IonItem lines="none" className="ion-no-margin ion-no-padding">
        <IonCard>
          <IonCardContent>
            Hi! I'm a software developer with at least 5 years of experience I have worked with Java, typescript, c# and python for mobile and web apps. 
          </IonCardContent>
        </IonCard>
      </IonItem>
    </>
  );
}